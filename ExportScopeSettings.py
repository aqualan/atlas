'''
Exports essential settings from the oscilloscope into a txt file

Usage: 	Execute directly with `python ExportScopeSettings.py`; the program will
		take care of the rest.

Arguments: 	None
'''

# For the 4034 scope in Mitch's Lab
# instrumentdescriptor = 'USB0::0x0699::0x040B::C021222::INSTR'

# For the 4054 scope in Mitch's Lab
instrumentdescriptor = 'USB0::0x0699::0x0401::C020278::INSTR'

import visa
import csv
import StringIO

# Which command group are we currently in?
commandGroup = ""

# Format and send out the command [item] to a file.
def writeout(output, item):
	# ':' signals for the scope to go back to its root, and start traversing a new command group
	if (item[0] == ':'): 
		output.write("\n") # End the previous group of commands
		commandGroup = item
		output.write(commandGroup + ';')
	else:
		output.write(item + ';')

# Ask the instrument for a set of parameters, returning them in a String array
def inquire(scope, output, parameter):
	settings = scope.ask(parameter)
	readback = StringIO.StringIO(settings)
	# Because the return values are ; separated, we will use that in place of commas for the csv reader, which will turn the readout into an array that is then worked on by writeout()
	reader = csv.reader(readback, delimiter=';')
	for array in reader:
		for item in array:
			writeout(output, item)

# martychangwhatdoido = The filename to be written to
def do(martychangwhatdoido, scope):
	# Pass the filename for writing
	#filename = raw_input("Please enter a file name to save to.\n>>> ")
	filename = martychangwhatdoido
	output = open(filename, 'w')
	# Add identifier to top of settings
	output.write("SCOPE\n")

	# Connect to the instrument
	# Turn on headers so that way the readouts can be parsed by eye
	scope.write('header on')

	# For all the following:
	# 1. Add a number if applicable for ImportSettings.py to find later
	# 2. Ask the instrument for settings, passing the result through inquire(scope, output, )
	# 3. After finishing a command group, add a new line character.
	output.write("===Horizontal Settings===\n")
	output.write("1.")
	inquire(scope, output, "horizontal?")
	output.write("\n")

	output.write("\n===Analog Channels===\n")
	output.write("2.")
	inquire(scope, output, "select:ch1?")
	inquire(scope, output, "ch1?")
	output.write("\n")

	output.write("\n3.")
	inquire(scope, output, "select:ch2?")
	inquire(scope, output, "ch2?")
	output.write("\n")

	output.write("\n4.")
	inquire(scope, output, "select:ch3?")
	inquire(scope, output, "ch3?")
	output.write("\n")

	output.write("\n5.")
	inquire(scope, output, "select:ch4?")
	inquire(scope, output, "ch4?")
	output.write("\n")

	output.write("\n===TriggerA===")
	output.write("\n6.")
	inquire(scope, output, "trigger:a:edge?")
	inquire(scope, output, "trigger:a:holdoff?")
	inquire(scope, output, "trigger:a:logic?")
	inquire(scope, output, "trigger:a:logic:input?")
	inquire(scope, output, "trigger:a:logic:pattern?")
	inquire(scope, output, "trigger:a:pulse?")
	inquire(scope, output, "trigger:a:runt?")
	inquire(scope, output, "trigger:a:sethold?")
	inquire(scope, output, "trigger:a:sethold:clock?")
	inquire(scope, output, "trigger:a:sethold:data?")
	inquire(scope, output, "trigger:a:transition?")
	inquire(scope, output, "trigger:a:video?")
	output.write("\n")

	# Get trigger B settings
	output.write("\n===TriggerB===")
	output.write("\n7.")
	inquire(scope, output, "trigger:b:edge?")
	inquire(scope, output, "trigger:b:events?")
	output.write("\n")

	# Get global Trigger settings("\n")
	output.write("\n===Trigger Global===")
	output.write("\n8.")
	inquire(scope, output, "trigger:external?")
	inquire(scope, output, "trigger:external:yunits?")
	inquire(scope, output, "trigger:frequency?")
	inquire(scope, output, "trigger:state?")
	output.write("\n")

	output.write("\n===Acquire Settings===")
	output.write("\n9.")
	inquire(scope, output, "acquire?")
	inquire(scope, output, "acquire:maxsamplerate?")
	inquire(scope, output, "acquire:numacq?")
	output.write("\n")

	output.write("\n===Math settings===")
	output.write("\n10.")
	inquire(scope, output, "math?")
	inquire(scope, output, "mathvar?")
	output.write("\n\n")

