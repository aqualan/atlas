#!/usr/bin/env python

'''
sillyscope.py

Function:       Holds the sillyscope class, which is the virtual
                oscilloscope class.

Classes:        sillyscope

Notes:          For now, the sillyscope class is only compatible
                with Tektronix oscilloscopes. However, it may be changed
                later to work with other kinds of oscilloscopes.
'''

from visa import instrument
from time import sleep
import numpy as np

import interpandtransform as interpol
from struct import unpack

class sillyscope(object):
    
    '''
    A virtual oscilloscope. This class holds the properties that grabbed
    from the oscilloscope, and is used to communicate with the oscilloscope.

    Functions: set_scope, get_waveforms
    '''

    def __init__(self, scope_name, numCh=4, parent=None):

        #---Initializing scope communicator. The communicator is used to read
        #and write messages to the oscilloscope.---#
        self.parent = parent
        self.communicator = instrument(scope_name)

        #---Initializing attributes of the scope.---#
        self.type = scope_name[6:20]
        self.write_headoff()
        
        self.numberChannels = numCh #Right now this scope only works with the four
                                #main channels of the oscilloscope, not math.
        self.impedanceStore = [] #List that holds the impedances for each channel.
        self.stateStore = [] #List that holds the on/off state for each channel.
        self.invertStore = [] #List that holds the inversion state for each channel.
        self.referenceStore = [] #List that holds that state of the reference waveform for each channel.
        self.mathState = '' #A string that determines
        self.mathExpression = ''

        #Obtaining properties from the oscilloscope.
        for i in range(self.numberChannels):

            if int(self.ask_channelimp(i + 1)) == 1:
                self.impedanceStore.append('MEG')
            else:
                self.impedanceStore.append('50.0E+0')

            if int(self.ask_channelstate(i + 1)) == 1:
                self.stateStore.append('ON')
            else:
                self.stateStore.append('OFF')

            if int(self.ask_channelinvert(i + 1)) == 1:
                self.invertStore.append('ON')
            else:
                self.invertStore.append('OFF')

            if int(self.ask_channelreferencestate(i + 1)) == 1:
                self.referenceStore.append('ON')
            else:
                self.referenceStore.append('OFF')

        if int(self.ask_mathstate()) == 1:
            self.mathState = 'ON'
        else:
            self.mathState = 'OFF'

        self.mathExpression = self.ask_mathexpression() #expression that is written on the scope's math channel

    #Below are the commands that are used to communicate with the oscilloscope.
    #Be warned all ye who enter.
    #
    #Note that commands that begin with the word "write" use visa's write command, while
    #commands that begin with the word "ask" use visa's ask command. An improved version
    #of visa's read, write, and ask commands have been included, which prevent the program
    #from experiencing an error if it tries to write a command after a device has been disconnected.

    def ask(self, message):
        #print message #uncomment this line if you want to see the messages written to the scope
        while self.parent.backend.oscilloscopeConnected:
            try: return self.communicator.ask(message)
            except: pass

    def ask_answer(self, channel):
        answer = float(self.ask('measurement:meas%d:value?' %(channel)).strip())
        if answer == None: return
        else: return answer

    def ask_channelimp(self, channel):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            imp = self.ask('CH' + str(channel) + ':TER?')
            if imp == None: return
            else: return imp.encode('ascii', 'ignore').strip()[0]

    def ask_channelinvert(self, channel):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            invert = self.ask('CH' + str(channel) + ':INVERT?')
            if invert == None: return
            else: return invert.encode('ascii', 'ignore').strip()[0]

    def ask_channelreferencestate(self, channel):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            state = self.ask('SELECT:REF' + str(channel) + '?')
            if state == None: return
            else: return state.encode('ascii', 'ignore').strip()[0]

    def ask_channelstate(self, channel):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            state = self.ask('SELECT:CH' + str(channel) + '?')
            if state == None: return
            else: return state.encode('ascii', 'ignore').strip()[0]

    def ask_errorstatus(self):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            error = self.ask('*ESR?')
            if error == None: return
            else: return error.encode('ascii', 'ignore').strip()

    def ask_mathexpression(self):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            expression = self.ask('MATH?')
            if expression == None: return
            else: return expression.encode('ascii', 'ignore').strip()

    def ask_mathstate(self):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            state = self.ask('SELECT:MATH?')
            if state == None: return
            else: return state.encode('ascii', 'ignore').strip()[0]

    def ask_measurementvalue(self, channel, meastype, DELAY):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('measurement:meas%d:type %s' %(channel, meastype))
            sleep(DELAY)
            return ask_answer(self, channel)

    def ask_measval(self, num):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            value = self.ask('measu:meas' + str(num) + ':val?')
            if value == None: return
            else: return float(value.strip())

    def ask_offs(self, channel):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            if channel is 'Math': return 0.0
            else: offs = self.ask('ch' + str(channel) + ':offs?')
            
            if offs == None: return
            else: return float(offs.strip())

    def ask_scale(self, channel):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            if channel is 'Math': scale = self.ask('MATH:VERT:SCALE?')
            else: scale = self.ask('ch' + str(channel) + ':scale?')
            if scale == None: return
            else: return float(scale.strip())

    def ask_searchtotal(self):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            total = self.ask('search:search1:total?')
            if total == None: return
            else: return total.strip()

    def ask_stddev(self, value):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            studmuffin = self.ask('measu:meas' + str(value) + ':std?')
            if studmuffin == None: return
            else: return float(studmuffin.strip())

    def ask_xincr(self):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            return float(self.ask('WFMOUTPRE:XINCR?'))

    def ask_ymult(self):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            return float(self.ask('WFMOUTPRE:YMULT?'))

    def ask_yoff(self):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            return float(self.ask('WFMOUTPRE:YOFF?'))

    def ask_yunits(self, channel):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            yunits = self.ask('CH' + str(channel) + ':YUNITS?')
            return yunits.encode('ascii', 'ignore').strip()

    def ask_yzero(self):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            return float(self.ask('WFMOUTPRE:YZERO?'))

    def get_waveforms(self):

        '''
        This function obtains the waveforms from each channel that is turned on on the
        oscilloscope.

        Arguments: None.
        '''
        
        waveformlist = [] #Resetting the list of current waveforms.
        timelist = []    #Resetting the list of waveform times.
        yunitslist = []
        
        channels = range(1, self.numberChannels + 1)
        channels.append('MATH')

        #Obtaining the waveform for each channel.
        for i in channels:
            self.write_headoff()

            channelstate = self.ask_mathstate() if i == 'MATH' else self.ask_channelstate(i)
            if int(channelstate) == 0:
                waveformlist.append(None)
                timelist.append(None)
                yunitslist.append(None)
                #If the channel is empty, None is stored in the list index for that channel.
            else:
                self.write_mathdatasource() if i == 'MATH' else self.write_datasource(i)
                self.write_datawidth(1)
                self.write_dataencodingrpb()

                ymult = self.ask_ymult()
                yzero = self.ask_yzero()
                yoff = self.ask_yoff()
                xincr = self.ask_xincr()
                self.write_headon()

                self.write_curve()
                data = self.read_raw()
                headerlen = 2 + int(data[7])
                header = data[:headerlen]
                ADC_wave = data[headerlen:-1]

                ADC_wave = np.array(unpack('%sB' % len(ADC_wave),ADC_wave))

                waveformlist.append((ADC_wave - yoff) * ymult  + yzero)
                timelist.append(np.arange(0, xincr * len(waveformlist[-1]), xincr))
                
                if i == 'MATH':
                    yunitslist.append(None)
                else:
                    yunits = self.ask_yunits(i)
                    yunitslist.append(yunits)



        return waveformlist, timelist, yunitslist

    # Load references from ASCII format to the oscilloscope
    # NOTE: Incoming waveforms should have their timevals in units of seconds
    def loadRefs(self, refDestination, datafilename, resolution=10000):
            # Specify which reference to load to
            self.set_datadestination(refDestination)

            # Read in data file and parse the two columns
            timevals_list = []
            wfrmvals_list = []
            for line in open(datafilename, 'r'):
                    try:
                            timeval, wfrmval = [float(value) for value in line.split()]
                            timevals_list.append(timeval)
                            wfrmvals_list.append(wfrmval)
                    except ValueError:
                            # If a header is encountered during the float cast, skip it.
                            continue

            # Pack values into dictionary to pass off for interpolation
            maxval = max(wfrmvals_list)
            minval = min(wfrmvals_list)
            rangeval = maxval - minval
            midval = maxval - rangeval/2
            packed = {'refform' : wfrmvals_list,
                              'timevals' : timevals_list,
                              'frequency' : 1, # 1/seconds(10^0)
                              'rangeref' : rangeval,
                              'midref' : midval,
                              'maxref' : maxval}

            # 32767 is the maximum value for a 2-byte input;
            # Returns a converted waveform.
            waveform = interpol.interpoldata(packed, maxdataval=32767.0)

            # Shift downwards to get inside of the 2-byte range [-32768, 32767]
            for i in range(len(waveform)):
                    waveform[i] = waveform[i] - 32767

            # Create Header
            # #<x><yyyy>
            # Start with a #
            # Then number of digits in <yyyy>
            # Then total number of bytes to transfer; because we use 2 bytes for
            # precision, it's the # of points * 2.
            header = '#' + str(len(str(resolution * 2))) + str(resolution * 2)
            # Create binary waveform
            binwaveform = pack('>'+'h'*len(waveform), *waveform)
            # TODO: Make sure it works.

            # Set signed integer data point representation
            self.set_byteformat("ri")

            # Specify length of waveform in # of points
            self.set_recordlength(resolution)

            # Specify data format (ASCII/Binary)
            self.set_dataformat("binary")

            # Specify number of bytes per data point
            self.set_numberofbytes(2)

            # Specify MSB byte order (Big-Endian)
            self.set_byteorder("MSB")

            # Always start from the first point
            self.set_startingpoint(1)

            # End on the last value
            self.set_stoppingpoint(resolution * 2)

            # Transfer
            self.transferdata(header+binwaveform)

    def read(self):
        while self.parent.backend.oscilloscopeConnected:
            try: return self.communicator.read().strip()
            except: pass

    def read_raw(self):
        return self.communicator.read_raw()

    def set_byteformat(self, format):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('wfminpre:bn_fmt %s' %(format))

    def set_byteorder(self, order):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('wfminpre:byt_or %s' %(order))

    def set_datadestination(self, refnum):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('data:destination ref%d' %(refnum))

    def set_dataformat(self, format):
        if (self.type =='0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('wfminpre:encdg %s' %(format))

    def set_gatingscreen(self):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('measurement:gating screen')

    def set_measurementsource(self, sourcenum, channel, source):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('measurement:meas%d:source%d %s' %(channel, sourcenum, source))

    def set_measurementstate(self, channel, state):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('measurement:meas%d:state %s' %(channel, state))

    def set_numberofbytes(self, bytes_per_point):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('wfminpre:byt_nr %d' %(bytes_per_point))

    def set_recordlength(self, nr_pts):
        if (self.type =='0x0699::040401') or (self.type == '0x0699::0x040B'):
            self.write('wfminpre:nr_pt %d' %(nr_pts))

    def set_reflevels(self, abslow, abshigh, DELAY):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('measurement:reflevel:method:absolut;' + 
                ' :measurement:reflevel:absolute:low %f; high %f'
                %(abslow, abshigh))

    def set_scope(self):
        '''
        This function sets the properties that have been indicated in the oscilloscope tab
        onto the oscilloscope.

        Arguments: None
        '''
        
        self.write_headoff()
        
        for i in range(self.numberChannels):
            self.write_channelimpedance(i + 1, self.impedanceStore[i])
            self.write_channelstate(i + 1, self.stateStore[i])
            self.write_channelinvert(i + 1, self.invertStore[i])
            self.write_channelreferencestate(i + 1, self.referenceStore[i])

        self.write_mathstate(self.mathState)
        self.write_mathexpression(self.parent.oscilloscopeTab.mathexpress.text())

    def set_startingpoint(self, starting_point):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('data:start %d' %(starting_point))
    
    def set_stoppingpoint(self, stopping_point):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('data:stop %d' %(stopping_point))            

    def transferdata(self, data):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('curve ' + data)

    def write(self, message):
        #print message #Uncomment this line to see the messages written to the scope
        while self.parent.backend.oscilloscopeConnected:
            try:
                self.communicator.write(message)
                break
            except: pass

    def write_acquiremode(self, mode):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('acq:mode ' + mode)

    def write_channelimpedance(self, channel, impedance):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('CH' + str(channel) + ':TER ' + str(impedance))

    def write_channelinvert(self, channel, invert):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('CH' + str(channel) + ':INVER ' + str(invert))

    def write_channelreferencestate(self, channel, state):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('SELECT:REF' + str(channel) + ' ' + str(state))

    def write_channelstate(self, channel, state):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            if channel is 'Math': self.write('SELECT:MATH ' + str(state))
            else: self.write('SELECT:CH' + str(channel) + ' ' + str(state))

    def write_curve(self):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            return self.write('CURVE?')

    def write_dataencodingrpb(self):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('DATA:ENC RPB')

    def write_datasource(self, channel):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('DATA:SOU CH' + str(channel))

    def write_datawidth(self, width):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('DATA:WIDTH ' + str(width))

    def write_deletemarks(self, which):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('mark:delete ' + str(which))

    def write_headoff(self):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('HEAD OFF')

    def write_headon(self):

        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('HEAD ON')

    def write_horizontaldelaytime(self, time):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('hor:del:tim ' + str(time))

    def write_horizontalrecord(self, length):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('hor:reco ' + str(length))

    def write_horizontalscale(self, scale):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('hor:sca ' + str(scale))

    def write_mathexpression(self, expression):
        expression = expression.encode('ascii', 'ignore').strip()
        
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('MATH:DEFINE \" ' + expression + '\"')

    def write_mathdatasource(self):

        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('DATA:SOU MATH')

    def write_mathstate(self, state):

        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('SELECT:MATH ' + str(state))

    def write_meassource(self, num, channel):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            if channel is 'Math': self.write('measu:meas' + str(num) + ':sou MATH')
            else: self.write('measu:meas' + str(num) + ':sou ch' + str(channel))

    def write_measstate(self, num, state):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            return self.write('measu:meas' + str(num) + ':state ' + str(state))

    def write_meastype(self, num, meastype):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('measu:meas' + str(num) + ':type ' + str(meastype))

    def write_numavgsamples(self, samples):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('acq:numav ' + str(samples))

    def write_offset(self, channel, offset):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            if channel is 'Math': return
            else: self.write('ch' + str(channel) + ':offs ' + str(offset))

    def write_position(self, channel, position):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            if channel is 'Math': self.write('MATH:pos ' + str(position))
            else: self.write('ch' + str(channel) + ':pos ' + str(position))    

    def write_scale(self, channel, scale):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            if channel is 'Math': self.write('MATH:VERT:SCALE ' + str(scale))
            else: self.write('ch' + str(channel) + ':scale ' + str(scale))

    def write_searchstate(self, value):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('search:search1:state ' + str(value))

    def write_selectmark(self, action):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('mark ' + str(action))

    def write_threshold(self, threshold, channel, value):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            if channel is 'Math': self.write('search:search1:trig:A:' + str(threshold) +
                                            ':MATH ' + str(value))
            else: self.write('search:search1:trig:A:' + str(threshold) + ':ch' +
                                    str(channel) + ' ' + str(value))

    def write_triggeredgesource(self, channel):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            if channel is "Math":
                self.write('search:search1:trig:A:edge:sou math')
            else:
                self.write('search:search1:trig:A:edge:sou ch' + str(channel))

    def write_triggerriseorfall(self, which):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('search:search1:trig:A:edge:slo ' + str(which))

    def write_triggertype(self, kind):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('search:search1:trig:A:type ' + str(kind))

    def write_zoommode(self, mode):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('zoo:mode ' + str(mode))

    def write_zoomstate(self, num, state):
        if (self.type == '0x0699::0x0401') or (self.type == '0x0699::0x040B'):
            self.write('zoo:zoom' + str(num) + ':state ' + str(state))
