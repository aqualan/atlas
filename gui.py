#!/usr/bin/env python

'''
gui.py

Function:           Holds the mainApplication class (the largest parent of the program) and
                    runs an instance of mainApplication.

Usage:              Run the file in order to launch the AMB Test program

Classes:            mainApplication

Notes:              
'''

import sys, os
from PySide.QtCore import(Qt, Slot, QUrl)
from PySide.QtGui import(QMainWindow, QTabWidget, QTabBar, QLabel, QMessageBox, QMenuBar, QMenu, QAction, QStatusBar,
                         QWidget, QPixmap, QHBoxLayout, QLayout, QDesktopServices, QIcon)
from time import sleep
from oscilloscope import oscilloscopeTab
from waveform import waveformTab
from linearityTab import linearityTab
from autoTab import autoTab
from resultsTab import linearityResultsTab
from backend import backend

class mainApplication(QMainWindow):
    
    '''
    mainApplication is the largest parent class of the AMB Test Controller application.
    The class holds the other subtabs/subclasses of the program, including the program's
    backend, oscilloscope tab, function generator tab, linearity test tab, auto test tab,
    virtual oscilloscope, virtual function generator, and other functionalities that may
    be added to later. mainApplication inherits properties from the PySide QMainWindow class.

    Functions: Error messages, closeEvent, 
    '''

    def __init__(self):

        #---Initializing the application.---#
        QMainWindow.__init__(self) #Initializing the inherited QWidget properties.
        self.setWindowTitle('Heptagon') #Setting the window title.
        self.setWindowIcon(QIcon(resource_path('icon.jpg')))
        self.backend = backend(self) #Creating the backend.
        self.move(0, 0) #Moving the widget to an appropriate section of the screen

        #---Generating widgets.---#

        self.tabBar = QTabWidget() #The tab bar that will hold other tabs.
        
        #The tabs that will be placed in the application.
        self.oscilloscopeTab = oscilloscopeTab(self) #The tab for the oscilloscope controller.
        self.AFGTab = waveformTab(self) #The tab for the function generator controller
        self.linearityTab = linearityTab(self) #The tab for the linearity test.
        self.autoTab = autoTab(self) #The tab for the auto test.

        #Adding each tab to the tab bar.
        self.tabBar.addTab(self.oscilloscopeTab, 'Oscilloscope Controller')
        self.tabBar.addTab(self.AFGTab, 'AFG Controller')
        self.tabBar.addTab(self.linearityTab, 'Linearity Test')
        self.tabBar.addTab(self.autoTab, 'Auto Test')

        #Setting tab properties
        self.tabBar.setMovable(1) #Allows the user to move tabs.
        self.tabBar.setTabsClosable(1) #Allows the user to close tabs.
        for i in range(4): self.tabBar.tabBar().tabButton(i, QTabBar.RightSide).resize(0, 0) #Makes the control/test tabs unclosable.
        self.tabBar.tabCloseRequested.connect(self.close_tab) #Function for closing tabs.

        #These values keep track of how many of each kind of results tab there is.
        self.nlintabs = 0
        self.ncaltabs = 0
        self.ndevtabs = 0
        
        #Messages at the bottom of the screen that let the user know
        #which devices have been connected.
        self.oscilloscopeState = QLabel('', self)
        self.AFGState = QLabel('', self)

        #Error message
        self.error = QMessageBox()

        #Connecting error signals from the backend to fucntion in mainApplication. Note that threads (such as backend) cannot call functions from a different thread/process
        self.backend.oscilloscopeError.connect(self.backend_oscerror)
        self.backend.AFGError.connect(self.backend_afgerror)
        self.backend.testQuitError.connect(self.backend_testquiterror)

        #Setting the tab bar as the window's central widget
        self.setCentralWidget(self.tabBar)

        #Generating the menu bar
        self.menuBar = QMenuBar()

        #Submenus and their respective buttons/functions
        fileMenu = QMenu('File')
        
        exitAction = QAction('Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.triggered.connect(self.close)

        fileMenu.addAction(exitAction)
        
        helpMenu = QMenu('Help')
        
        readmeAction = QAction('View Readme', self)
        readmeAction.triggered.connect(self.open_readme)
        readmeAction.setShortcut('Ctrl+H')
        tutorialAction = QAction('View Operation Manual', self)
        tutorialAction.triggered.connect(self.open_manual)
        tutorialAction.setShortcut('Ctrl+T')
        aboutAction = QAction('About HEPTAGON', self)
        aboutAction.triggered.connect(self.show_about)
        
        helpMenu.addAction(readmeAction)
        helpMenu.addAction(tutorialAction)
        helpMenu.addSeparator()
        helpMenu.addAction(aboutAction)
        
        self.menuBar.addMenu(fileMenu)
        self.menuBar.addMenu(helpMenu)
        self.setMenuBar(self.menuBar)

        #Generating the status bar
        self.statusBar = QStatusBar()
        self.statusBar.addPermanentWidget(self.oscilloscopeState)
        self.statusBar.addPermanentWidget(self.AFGState)
        self.setStatusBar(self.statusBar)

        #---Running the program backend.---#
        self.backend.start()

    #Below are all of the error messages shown by the program's error window
    def scope_nofileerror(self):
        self.error.warning(self, 'No File Loaded', 'You have not loaded a file for writing \na reference waveform to the oscilloscope.')

    def general_nameerror(self):
        self.error.warning(self, 'Incorrect File Input', 'There was an error attempting to read the ' +
                           'selected file. Please make sure that the file that was selected is ' +
                           'formatted correctly and try again.')

    @Slot()
    def backend_oscerror(self):
        self.error.critical(self, 'Oscilloscope Error', 'There was an error connecting the oscilloscope. ' +
                            '\n Please try disconnecting the oscilloscope and then reconnecting it.')

    @Slot()
    def backend_afgerror(self):
        self.error.critical(self, 'Function Generator Error', 'There was an error connecting to the function generator. ' +
                            '\n Please try disconnecting the function generator and then reconnecting it.')

    @Slot()
    def backend_testquiterror(self):
        self.linearityTab.quit_test()
        self.error.critical(self, 'Test Stopped', 'The test has been stopped because the oscilloscope or function generator have been disconnected ' +
                            'or encountered an error during testing.')

    def auto_channelerror(self):
        self.error.warning(self, 'Too Many Channels', 'You may only use up to 4 channels in an auto test. Please deselect ' +
                           'one of the channel checkboxes.')

    def auto_riseterror(self):
        self.error.warning(self, 'Incorrect Risetime', 'Percent risetime must be a decimal value between 0 and 100.')

    def auto_amplitudeerror(self):
        self.error.warning(self, 'Incorrect Amplitude', 'Percent amplitude must be a decimal value between 0 and 100.')

    def auto_phaseerror(self):
        self.error.warning(self, 'Incorrect Phase', 'Percent phase must be a decimal value between 0 and 100.')

    def afg_nofileerror(self):
        self.error.warning(self, 'Select File', 'Must select a file for loading an arb to the fucntion generator.')

    def afg_waveformerror(self):
        self.error.warning(self, 'Select Waveform Type', 'Must select an option for waveform type.')

    def afg_swedenerror(self):
        self.error.warning(self, 'Select Burst Mode', 'Must select an option for burst mode.')

    def afg_tunitserror(self):
        self.error.warning(self, 'Select Time Units', 'Must select an option for time units.')

    def afg_vunitserror(self):
        self.error.warning(self, 'Select Voltage Units', 'Must select an option for voltage units.')

    def afg_resolutionerror(self):
        self.error.warning(self, 'Incorrect Resolution', 'Resolution must be a positive integer.')

    def afg_frequencyerror(self):
        self.error.warning(self, 'Incorrect Frequency', 'Frequency must be a positive number.')

    def afg_offseterror(self):
        self.error.warning(self, 'Incorrect Vertical Offset', 'Vertical offset must be a number.')

    def afg_amplitudeerror(self):
        self.error.warning(self, 'Incorrect Amplitude', 'Amplitude must be a positive number.')

    def linearity_nofileerror(self):
        self.error.warning(self, 'No File Loaded', 'No file has been loaded for running a calibration/device test.')

    def linearity_waveerror(self):
        self.error.warning(self, 'No File Loaded', 'No file has been loaded for sending an arb to the function generator.')

    def linearity_startverror(self):
        self.error.warning(self, 'Start Voltage', 'Must indicate a start voltage.')

    def linearity_startvtypeerror(self):
        self.error.warning(self, 'Start Voltage', 'Start voltage must be a decimal number.')

    def linearity_endverror(self):
        self.error.warning(self, 'End Voltage', 'Must indicate an end voltage.')

    def linearity_endvtypeerror(self):
        self.error.warning(self, 'End Voltage', 'End voltage must be a decimal number.')

    def linearity_reserror(self):
        self.error.warning(self, 'Test Resolution', 'Must indicate a test resolution.')

    def linearity_restypeerror1(self):
        self.error.warning(self, 'Test Resolution', 'Test resolution must be greater than 0.')

    def linearity_restypeerror2(self):
        self.error.warning(self, 'Test Resolution', 'Test resolution must be an integer.')

    def linearity_outputerror(self):
        self.error.warning(self, 'Output Flag', 'Must select an option for output flag.')

    def linearity_advancederror(self):
        self.error.warning(self, 'Outut Flag', 'Output for advanced flag has invalid syntax.')

    def linearity_oscchanerror(self):
        self.error.warning(self, 'Oscilloscope Channel', 'Must select an option for oscilloscope channel.')

    def linearity_afgchanerror(self):
        self.error.warning(self, 'Function Generator Channel', 'Must select an option for function generator channel.')

    def linearity_tolerancetypeerror(self):
        self.error.warning(self, 'Tolerance', 'Tolerance must be a number.')

    def linearity_toleranceerror(self):
        self.error.warning(self, 'Tolerance', 'Tolerance must be a positive number.')

    def linearity_samplestypeerror(self):
        self.error.warning(self, 'Samples', 'Number of samples must be an integer.')

    def linearity_sampleserror(self):
        self.error.warning(self, 'Samples', 'Number of samples must be a power of 2.')

    def linearity_testerror(self):
        self.error.critical(self, 'Test Failed', 'Test encountered the following error: \n' +
                               self.linearityTab.linresults)

    def linearity_selecterror(self):
        self.error.warning(self, 'Select Test', 'Please select a test to run.')

    def linearity_matherror(self):
        self.error.warning(self, 'Select Math Operation', 'Please select an operation for the math channel.')

    def linearity_matherror2(self):
        self.error.warning(self, 'Must Select Unique Channels', 'You must select unique channels for the math channel to operate on.')

    @Slot()
    def closeEvent(self, event):

        '''
        Arguments - Event: The close event.

        closeEvent attempts to close the program. It checks to see if there are any tests running or unsaved results tabs,
        and asks for confirmation from the user if either of these conditions are met before shutting down.
        '''

        #For loop checks through each tab that is open in the tab bar.
        for i in range(self.tabBar.count()):

            #Checking if a tab is running a test.
            if self.tabBar.widget(i).kind == 'test' and (self.tabBar.widget(i).threadState == 'on' or self.tabBar.widget(i) == 'done'):
                self.tabBar.setCurrentWidget(self.tabBar.widget(i))
                reply = QMessageBox.warning(self, 'Test Running', "A test is currently running. " +
                                             "Are you sure you want to quit?", QMessageBox.Yes | QMessageBox.No,
                                             QMessageBox.No)
                if reply == QMessageBox.Yes:
                    pass
                else:
                    event.ignore()
                    return
            #Checking if a tab is an unsaved result.
            elif self.tabBar.widget(i).kind == 'results' and not self.tabBar.widget(i).exported:
                self.tabBar.setCurrentWidget(self.tabBar.widget(i))
                reply = QMessageBox.question(self, 'Save Results?', 'The data from this test has not been saved yet.\n' +
                                             'Would you like to save the results from this tab?', QMessageBox.Save |
                                             QMessageBox.Discard | QMessageBox.Cancel, QMessageBox.Save)
                if reply == QMessageBox.Save:
                    self.tabBar.widget(i).export_results()
                    if self.tabBar.widget(i).exported: self.tabBar.widget(i).deleteLater()
                    else:
                        event.ignore()
                        return
                elif reply == QMessageBox.Discard: self.tabBar.widget(i).deleteLater()
                else:
                    event.ignore()
                    return
            else: event.accept()
        event.accept()

    @Slot()
    def close_tab(self, index):

        '''
        Requests a tab close from the tab at the given index.
        '''
        self.tabBar.widget(index).close_tab()
        
    @Slot()
    def new_linearity_results(self):

        '''
        Generates a new resuls page from a linearity test by creating a new instance of the linearityResultsTab
        and placing it in the tab bar.
        '''

        which = self.linearityTab.thread.type #Determining what kind of test was run.
        self.linearityTab.quit_test() #Shutting down the test thread.

        if type(self.linearityTab.linresults) == str: #Emits an error if the results of the test are a string (they should be a dictionary)
            self.linearity_testerror()
            return

        #For each test, the counter for that kind of test is increased, a tab for the test is generated, and it is added to the tab bar.
        if which == 'Linearity':
            self.nlintabs += 1
            tab = linearityResultsTab(which, self.linearityTab.linresults, self.nlintabs, parent=self.tabBar)
            self.tabBar.addTab(tab, '%s Results %s' % (which, str(self.nlintabs)))
        elif which == 'Calibration':
            self.ncaltabs += 1
            tab = linearityResultsTab(which, self.linearityTab.linresults, self.ncaltabs, parent=self.tabBar)
            self.tabBar.addTab(tab, '%s Results %s' % (which, str(self.ncaltabs)))
        else:
            self.ndevtabs += 1
            tab = linearityResultsTab(which, self.linearityTab.linresults, self.ndevtabs, parent=self.tabBar)
            self.tabBar.addTab(tab, '%s Results %s' % (which, str(self.ndevtabs)))
        self.tabBar.setCurrentWidget(tab) #Displaying the new tab.
        self.tabBar.setTabsClosable(1)

    @Slot()
    def new_auto_results(self):

        '''
        Generates a new results page from an auto test.

        Note: the auto test is currently incomplete, and so this function must be filled out later.
        '''

        which = self.autoTab.thread.type
        self.autoTab.quit_test()

    @Slot()
    def show_about(self):

        '''
        Shows the about page for the program.
        '''

        self.aboutWindow = QWidget()
        self.aboutWindow.setWindowModality(Qt.ApplicationModal)

        pixmap = QPixmap(resource_path('credits.jpg'))
        label = QLabel()
        label.setPixmap(pixmap)

        aboutLayout = QHBoxLayout()
        aboutLayout.addWidget(label)
        aboutLayout.setSizeConstraint(QLayout.SetFixedSize)
        self.aboutWindow.setLayout(aboutLayout)

        self.aboutWindow.show()

    @Slot()
    def open_manual(self):

        '''
        Opens the operation manual from the website.
        '''

        QDesktopServices.openUrl(QUrl('https://sites.google.com/site/pennheptagon/operation-manual'))

    @Slot()
    def open_readme(self):

        '''
        Opens the readme from the GIT.
        '''
        
        QDesktopServices.openUrl(QUrl('https://bitbucket.org/aqualan/heptagon-release'))

#This function allows PyInstaller to include image files when it builds binaries.
#You should use this function whenever you use an outside image file in the program.
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

#For running the program from file.
if __name__ == "__main__":
    import sys
    from PySide.QtGui import QApplication

    #Creating the application
    qtApp = QApplication(sys.argv) #Creating an instance of PySide's QApplication.
    application = mainApplication() #Creating an instance of mainApplication.
    application.show() #Showing the application.
    qtApp.exec_() #Executing the application.

    #Closing the backend of the application once the main application has closed.
    if application.linearityTab.threadState == 'on' or application.linearityTab.threadState == 'done':
        application.linearityTab.quit_test()
    
    application.backend.running = False
    sleep(0.2)
    application.backend.exit()

    #Ending program.
    sys.exit()
