# -*- mode: python -*-
a = Analysis(['gui.py'],
             pathex=['C:\\Users\\Alex\\Documents\\Penn\\Code'],
             hiddenimports=['scipy.special._ufuncs_cxx'],
             hookspath=None,
             runtime_hooks=None)
for d in a.datas:
	if 'pyconfig' in d[0]:
		a.datas.remove(d)
		break
a.datas += [('Images/icon.jpg', 'C:\\Users\\Alex\\Documents\\Penn\\Code\\icon.jpg', 'DATA')]
a.datas += [('Images/credits.jpg', 'C:\\Users\\Alex\\Documents\\Penn\\Code\\credits.jpg', 'DATA')]
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='Heptagon.exe',
          debug=False,
		  icon='favicon.ico',
          strip=None,
          upx=True,
          console=True)
