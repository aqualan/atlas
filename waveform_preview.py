#!/usr/bin/env python

'''
waveform_preview.py

Function:    Holds the waveformPreview class, which is used to
                    display graphs of various kinds in the program

Classes:            waveformPreview

Notes:              For the future, it might be helpful to make spacing in the graphs
                    more economical.
'''

from matplotlib import(use, rcParams, ticker)
from numpy import(sin, linspace, array, pi, e)

use('Qt4Agg')
rcParams['backend.qt4']='PySide'

import pylab

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

rcParams['figure.facecolor'] = 'w' #This command makes the space
                                              #around the figure white.

class waveformPreview(FigureCanvas):

    '''
    This class is used to create graphs in the application. It holds a group
    of premade waveforms that can be used to preview non-arbs.

    Functions: waveformPlot, oscilloscopePlot, linearityPlot, deviationPlot, fullscalePlot
    '''
    
    def __init__(self, parent=None):

        #---Initializing---#
        FigureCanvas.__init__(self, Figure())
        self.setParent(parent)

        #---Creating figure/object---#
        self.figure = Figure()
        self.figure.set_tight_layout(True) #Keeps space surrounding figure to a minimum
        self.canvas = FigureCanvas(self.figure)
        self.axes = self.figure.add_subplot(111, axis_bgcolor='black')
        self.formatter = ticker.ScalarFormatter(useOffset=False)

        #---Pre-made waveforms---#
        self.Sine = 0.5*sin(linspace(0.0, 2.0*pi, num=100))
        self.Sinetime = linspace(0.0, 1.0, num=100)

        self.Square = array([0.01, 0.51, 0.51, -0.51, -0.51, 0.01])
        self.Squaretime = array([0.003, 0.003, 0.5, 0.5, 0.997, 0.997])

        self.Ramp = array([0.0, 0.5, 0.0, -0.5, 0.0])
        self.Ramptime = array([0, 0.25, 0.50, 0.75, 1.0])

        self.Pulse = self.Square
        self.Pulsetime = self.Squaretime

        self.Gaussiantime = linspace(0.0, 1.0, num=100)
        self.Gaussian = e**(-(linspace(-3.0, 3.0, num=100))**2)

        self.EDecay = e**(-linspace(0, 5, num=100))
        self.EDecaytime = linspace(0.0, 1.0, num=100)

        self.ERise = 1-e**(-linspace(0, 5, num=100))
        self.ERisetime = linspace(0.0, 1.0, num=100)

    def waveformPlot(self, wavetype=None, time=[1], amplitude=[1], invert=False):

        '''
        waveformPlot is used to plot the preview of the outgoing waveform for the function
        generator. Right now, it is used in the function generator tab and the linearity
        test tab.

        Arguments:
        
            wavetype: The kind of wave being sent (Sine, Square, Arb, etc.)
            
            time: The list of time values for the waveform being previewed.
            
            amplitude: The list of amplitude values for the waveform being previewed.
        '''

        if wavetype == 'Arb' and invert:
            amplitude = array(amplitude) * -1
        elif (wavetype == 'Arb' and not invert) or wavetype == 'Nonarb':
            amplitude = array(amplitude)
        else:
            pass
            
        self.axes.clear() #Clearing the figure from what was previously being displayed.
        self.axes.plot(time, amplitude, 'r', linewidth=2.0)

        #Turning on grid
        self.axes.minorticks_on()
        self.axes.grid(True, which='major', color='green', linewidth=1, linestyle='-')
        self.axes.grid(True, which='minor', color='green', linewidth=1)

        #Formatting axes
        self.axes.ticklabel_format(axis='x', style='sci', scilimits=(0,0))#uses scientific notation
        self.axes.ticklabel_format(axis='y', style='sci', scilimis=(0,0))
        if wavetype == None: #Turns off axes labels for an empty graph
            self.axes.xaxis.set_ticklabels([])
            self.axes.yaxis.set_ticklabels([])
        self.axes.set_xlabel('Time')
        self.axes.set_ylabel('Amplitude')

        self.canvas.draw() #Displaying waveform

    def oscilloscopePlot(self, color=None, time=[1], amplitude=[1], vunits='V', tunits='s'):

        '''
        oscilloscopePlot is used to plot the preview the incoming waveforms in the
        oscilloscope tab. Since the oscilloscope graph is used to present more than
        one waveform, oscilloscopePlot is called several times from the oscilloscope
        tab. The plot is cleared from there.

        Arguments:

            color: The color of the waveform being shown. For an empty graph, color
            is passed as None, and nothing visible is plotted on the graph.
            
            time: The list of time values for the waveform being previewed.

            amplitude: The list of amplitude values for the waveform being previewed.

            vunits: The voltage units.

            tunits: The time units.
        '''

        if color != None: #Plotting a wave
            self.axes.plot(time, amplitude, color, linewidth=2.0)
        else: #Plotting an empty graph
            self.axes.plot(time, amplitude, linewidth=2.0)

        #Turning on grid
        self.axes.minorticks_on()
        self.axes.grid(True, which='major', color='green', linewidth=1, linestyle='-')
        self.axes.grid(True, which='minor', color='green', linewidth=1)

        #Formatting axes
        self.axes.ticklabel_format(axis='x', style='sci', scilimits=(0,0))
        self.axes.set_xlabel('Time (%s)' % tunits)
        self.axes.set_ylabel('Amplitude (%s)' % vunits)
        if color == None: #Turns off axes for an empty graph
            self.axes.xaxis.set_ticklabels([])
            self.axes.yaxis.set_ticklabels([])
        self.canvas.draw() #Displaying waveform

    def linearityPlot(self, slope=None, intercept=[1], inputsig=[1], outputsig=[1]):

        '''
        linearityPlot is used to display the results of the linearity test.

        Arguments:

            slope: The slope of the best-fit line calculated for the linearity test data.
            For an empty graph, slope is passed as None.

            intercept: The y-intercept of the best-fit line calculated for the linearity test
            data.

            inputsig: The list of input signals taken from the test.

            outputsig: The list of output signals taken from the test.
        '''

        self.axes.clear()

        if slope == None: #For an empty graph
            bfx = [0]
            bfy = [0]
        else:
            bfx = linspace(0, max(inputsig), num=100)
            bfy = (slope * bfx) + intercept

        #Plotting the data        
        self.axes.plot(inputsig, outputsig, 'ob', bfx, bfy, 'r')

        #Formatting the graph
        self.axes.set_axis_bgcolor('white')
        self.axes.grid(True, which='major', color='black', linewidth=1, linestyle='-')
        self.axes.set_title('Input/Output vs. Linear Regression')
        self.axes.set_xlabel('Input Voltage (V)')
        self.axes.set_ylabel('Output Voltage (V)')
        if slope == None: #Removing axes for an empty graph
            self.axes.xaxis.set_ticklabels([])
            self.axes.yaxis.set_ticklabels([])
        self.canvas.draw() #Displaying data

    def deviationPlot(self, inputvals, deviation):

        '''
        This function is used to display the percent deviation plots in linearityResultsTab.

        Arguments:

            inputvals: The values from the input signal.

            deviation: The deviation from regression calculated at each input value.
        '''

        self.axes.plot(inputvals, deviation, 'ob')

        self.axes.set_axis_bgcolor('white')
        self.axes.grid(True, which='major', color='black', linewidth=1, linestyle='-')
        self.axes.set_title('Percent Deviation from Regression')
        self.axes.set_xlabel('Input Voltage (V)')
        self.axes.set_ylabel('Deviation (%)')
        self.canvas.draw()

    def fullscalePlot(self, inputvals, deviation):

        '''
        This function is used to display the full scale deviation plots in linearityResultsTab.

        Arguments:

            inputvals: The values from the input signal.

            deviation: The full scale deviation from regression calculated at each input value.
        '''

        self.axes.plot(inputvals, deviation, 'og')

        self.axes.set_axis_bgcolor('white')
        self.axes.grid(True, which='major', color='black', linewidth=1, linestyle='-')
        self.axes.set_title('Full Scale Deviation from Regression')
        self.axes.set_xlabel('Input Voltage (V)')
        self.axes.set_ylabel('Full Scale Deviation (%)')
        self.canvas.draw()

#Used for reviewing the graph in isolation from the program
if __name__ == '__main__':

    from PySide.QtCore import *
    from PySide.QtGui import *
    import sys

    qtApp = QApplication(sys.argv)
    preview = waveformPreview()
    preview.waveformPlot(wavetype='EDecay')
    preview.show()
    qtApp.exec_()
