'''
Imports essential settings from a file into the afg

Usage: 	Execute directly with `python ImportAFGSettings.py`; the program will
		take care of the rest.

Arguments: 	None
'''

# For the 3252 in Mitch's Lab
DESC3252 = 'USB0::0x0699::0x0345::C020930::INSTR'

# For the Agilent 81150A
DESC81150 = "USB0::0x0957::0x4108::DE47C00187::INSTR"

import visa
import StringIO
import csv
import sys

# Based on a user answer, pick which instrument descriptor to use.
def chooseInstrument():
	print("Please select a function generator to use:")
	print("1) Tektronix 3252")
	print("2) Agilent 81150A")

	while(True):
		userInput = raw_input(">>> ")
		if (userInput == "1" or userInput == "1)"):
			return DESC3252
		elif (userInput == "2" or userInput == "2)"):
			return DESC81150
		else:
			print("\nInput invalid.\n")

# Method for easy sending of commands in a command group
def parse(afg, lines, index):
	while lines[index] != "":
		# Write command at row [index] to the afg
		#print(lines[index])
		afg.write(lines[index])
		index += 1

def setupSource3252(afg, lines, channel, index):
	#print("Setting source%d settings from line %d..." %(channel, index))
	while lines[index] != "":
		# Check if an output state is 0; if so, skip it, else the actino will
		# default the AFG to continuous mode.
		if (lines[index] == "source1:pm:state 0" or lines[index] == "source1:pwm:state 0" or lines[index] == "source1:am:state 0" or lines[index] == "source1:fm:state 0" or lines[index] == "source1:burst:state 0" or lines[index] == "source1:fskey:state 0"):
			pass
		elif (lines[index] == "source2:pm:state 0" or lines[index] == "source2:pwm:state 0" or lines[index] == "source2:am:state 0" or lines[index] == "source2:fm:state 0" or lines[index] == "source2:burst:state 0" or lines[index] == "source2:fskey:state 0"):
			pass
		else:
			# Write command at row[index] to the scope
			afg.write(lines[index])
		index += 1

# martychangwhatdoido = the file to read from
def do(martychangwhatdoido, afginstance):
	#filename = raw_input("Please enter a filename to import from.\n>>> ")
	filename = martychangwhatdoido
	settings = open(filename, 'r')
	# Connect to instrument
	rm = visa.ResourceManager()
	'''
	If this ever becomes more universal, uncomment the following.
	'''
	#instrumentdescriptor = chooseInstrument()
	instrumentdescriptor = DESC3252
	#afg = rm.get_instrument(instrumentdescriptor)
	afg = afginstance
	# Array with the line-by-line elements
	lines = [line.strip() for line in settings]

	# Main method
	i = 0 # Line index
	if (instrumentdescriptor == DESC3252):
		for line in lines:
			if(line == "1."):
				#print("Setting output parameters from line %d..." %(i+1))
				parse(afg, lines, i)
			elif(line == "2."):
				setupSource3252(afg, lines, 1, i+1)
			elif(line == "3."):
				setupSource3252(afg, lines, 2, i+1)
			else:
				pass
			i = i+1
	elif (instrumentdescriptor == DESC81150):
		for line in lines:
			if (line == "1."):
				#print("\nSetting Apply settings on 1...")
				parse(afg, lines, i+1)
			elif (line == "2."):
				#print("\nSetting Apply settings on 2...")
				parse(afg, lines, i+1)
			elif (line == "3."):
				#print("\nSetting Arbitrary settings on 1...")
				parse(afg, lines, i+1)
			elif (line == "4."):
				#print("\nSetting Arbitrary settings on 2...")
				parse(afg, lines, i+1)
			elif (line == "5."):
				#print("\nSetting Arbitrary settings for both")
				parse(afg, lines, i+1)
			elif (line == "6."):
				#print("\nSetting Burst settings on 1...")
				parse(afg, lines, i+1)
			elif (line == "7."):
				#print("\nSetting Burst settings on 2...")
				parse(afg, lines, i+1)
			elif (line == "8."):
				#print("\nSetting Burst settings on both...")
				parse(afg, lines, i+1)
			elif (line == "9."):
				#print("\nSetting Level Commands on 1...")
				parse(afg, lines, i+1)
			elif (line == "10."):
				#print("\nSetting Level Commands on 2...")
				parse(afg, lines, i+1)
			elif (line == "11."):
				#print("\nSetting Amplitude Modulation Commands on 1...")
				parse(afg, lines, i+1)
			elif (line == "12."):
				#print("\nSetting Amplitude Modulation Commands on 2...")
				parse(afg, lines, i+1)
			elif (line == "13."):
				#print("\nSetting Frequency Modulation Commands on 1...")
				parse(afg, lines, i+1)
			elif (line == "14."):
				#print("\nSetting Frequency Modulaton Commands on 2...")
				parse(afg, lines, i+1)
			elif (line == "15."):
				#print("\nSetting Frequency Shift-Keying Modulation Commands on 1...")
				parse(afg, lines, i+1)
			elif (line == "16."):
				#print("\nSetting Freqneucy Shift-Keying Modulation Commands on 2...")
				parse(afg, lines, i+1)
			elif (line == "17."):
				#print("\nSetting Phase Modulation Commands on 1...")
				parse(afg, lines, i+1)
			elif (line == "18."):
				#print("\nSetting Phase Modulation Commands on 2...")
				parse(afg, lines, i+1)
			elif (line == "19."):
				#print("\nSetting Pulse Width Modulation Commands on 1...")
				parse(afg, lines, i+1)
			elif (line == "20."):
				#print("\nSetting Pulse Width Modulation Commands on 2...")
				parse(afg, lines, i+1)
			elif (line == "21."):
				#print("\nSetting Channel Commands...")
				parse(afg, lines, i+1)
			elif (line == "22."):
				#print("\nSetting Output Commands on 1...")
				parse(afg, lines, i+1)
			elif (line == "23."):
				#print("\nSetting Output Commands on 2...")
				parse(afg, lines, i+1)
			elif (line == "24."):
				#print("\nSetting Output Commands for both...")
				parse(afg, lines, i+1)
			elif (line == "25."):
				#print("\nSetting Output Function Commands on 1...")
				parse(afg, lines, i+1)
			elif (line == "26."):
				#print("\nSetting Output Function Commands on 2...")
				parse(afg, lines, i+1)
			elif (line == "27."):
				#print("\nSetting Reference Clock Commands...")
				parse(afg, lines, i+1)
			elif (line == "28."):
				#print("\nSetting Sweep Commands on 1...")
				parse(afg, lines, i+1)
			elif (line == "29."):
				#print("\nSetting Sweep Commands on 2...")
				parse(afg, lines, i+1)
			elif (line == "30."):
				#print("\nSetting Trigger Commands on 1...")
				parse(afg, lines, i+1)
			elif (line == "31."):
				#print("\nSetting Trigger Commands on 2...")
				parse(afg, lines, i+1)
			elif (line == "32."):
				#print("\nSetting Trigger Commands for both...")
				parse(afg, lines, i+1)
			elif (line == "33."):
				#print("\nSetting Pattern Related Commands on 1...")
				parse(afg, lines, i+1)
			elif (line == "34."):
				#print("\nSetting Pattern Related Commands on 2...")
				parse(afg, lines, i+1)
			elif (line == "35."):
				#print("\nSetting Pattern Related Commands for both...")
				parse(afg, lines, i+1)
			elif (line == "SCOPE"):
				#print("You tried to load Scope settings into the AFG. Don't do that.")
				sys.exit()
			else:
				pass
			i = i+1

	#print("%d lines parsed, afg is ready to go!" %(i))
