#!/usr/bin/env python

'''
waveform.py

Function:   Holds the waveformTab class, which holds the widgets and layout
            for the function generator tab of mainApplication.

Classes:    waveformTab
'''

import sys
from PySide.QtCore import(Qt, Slot)
from PySide.QtGui import(QWidget, QLabel, QComboBox, QPushButton, QHBoxLayout, QLineEdit, QCheckBox, QApplication,
                         QGroupBox, QFormLayout, QTableWidget, QTableWidgetItem, QVBoxLayout, QFileDialog)

from waveform_preview import waveformPreview
from readfromfile import importdata
from loadwaveform import loadwaveform
from ImportAFGSettings import do as import_settings
from ExportAFGSettings import do as export_settings

class waveformTab(QWidget):
    
    '''
    The tab for controlling/communicating with the function generator.

    Functions: save_config, load_config, send_wave, choose_waveform, load_wave, set_table_screen, set_units
    '''
    
    kind = 'controller'

    def __init__(self, parent=None):

        #---Initializing---#
        QWidget.__init__(self, parent)
        self.parent = parent

        #---Top Row: Choosing waveform---#

        #Generating widgets
        self.chooseName = QLabel('Choose waveform: ')

        #Combo box for choosing the waveform type
        self.chooseWaveBox = QComboBox()

        #Dictionary for referencing the command name of the wavetype
        self.chooseWaveDict = {'Sine': 'sin', 'Square': 'squ', 'Ramp': 'ramp', 'Pulse': 'puls',
                               'Arb': 'ememory', 'Gaussian': 'gaus',
                               'ERise': 'eris', 'EDecay': 'edec'}
        self.chooseWaveBox.addItems(['Sine', 'Square', 'Ramp', 'Pulse', 'Arb', 'Gaussian', 'ERise',
                                     'EDecay'])
        self.chooseWaveBox.setCurrentIndex(-1)
        self.chooseWaveBox.currentIndexChanged.connect(self.choose_waveform)

        #Push button for loading an arbitrary waveform from file. The button is only enabled
        #if 'Arb' is selected in chooseWaveBox
        self.getWaveform = QPushButton('Load Waveform')
        self.loadedWave = False #A boolean for determing if a wave has been loaded from file.
        self.getWaveform.setDisabled(1)
        self.getWaveform.clicked.connect(self.load_wave)

        #Organizing widgets
        self.chooseLayout = QHBoxLayout()
        self.chooseLayout.addWidget(self.chooseName)
        self.chooseLayout.addWidget(self.chooseWaveBox)
        self.chooseLayout.addWidget(self.getWaveform)
        self.chooseLayout.addStretch(1)

        #---Middle Row: Waveform data/settings---#
        #Generating widgets

        #Creating settings box

        #A combo box for setting burst mode
        self.channelBox = QComboBox()
        self.channelBox.addItems(['1', '2'])

        self.swedenBox = QComboBox()
        self.swedenBox.addItems(['Yes', 'No'])
        self.swedenBox.setCurrentIndex(-1)

        #Box for setting the waveform resolution
        self.resolutionBox = QLineEdit()

        #Combo box for setting the waveform time units
        self.timeBox = QComboBox()
        self.timeDict = {'Seconds (s)': 's', 'Milliseconds (ms)': 'ms',
                               'Microseconds (us)': 'us', 'Nanoseconds (ns)': 'ns'}

        #Dictionary for referencing the argument passed to loadwaveform for time units
        self.timeBox.addItems(['Seconds (s)', 'Milliseconds (ms)',
                               'Microseconds (us)', 'Nanoseconds (ns)'])
        self.timeBox.setCurrentIndex(-1)
        self.timeBox.currentIndexChanged.connect(self.set_units)

        #Combo box for setting the waveform volt units         
        self.voltBox = QComboBox()
        self.voltDict = {'Volts (V)': 'V', 'Millivolts (mV)': 'mV'}
        self.voltBox.addItems(['Volts (V)', 'Millivolts (mV)'])
        self.voltBox.setCurrentIndex(-1)
        self.voltBox.currentIndexChanged.connect(self.set_units)

        self.amplitudeBox = QLineEdit() #Box for setting the waveform amplitude
        self.amplitudeBox.textChanged.connect(self.set_table_screen)

        self.offsetBox = QLineEdit() #Box for setting the waveform offset
        self.offsetBox.textChanged.connect(self.set_table_screen)

        self.frequencyBox = QLineEdit() #Box for setting the waveform frequency
        self.frequencyBox.textChanged.connect(self.set_table_screen)

        self.invertBox = QCheckBox() #Box for setting the invert state
        self.invertBox.toggled.connect(self.set_table_screen)

        #Formatting settings box
        self.settingsBox = QGroupBox('Channel Settings')
        self.settingsLayout = QFormLayout()
        self.settingsLayout.addRow('Channel: ', self.channelBox)
        self.settingsLayout.addRow('Burst: ', self.swedenBox)
        self.settingsLayout.addRow('Resolution: ', self.resolutionBox)
        self.settingsLayout.addRow('Time Units: ', self.timeBox)
        self.settingsLayout.addRow('Volt Units: ', self.voltBox)
        self.settingsLayout.addRow('Amplitude: ', self.amplitudeBox)
        self.settingsLayout.addRow('Vertical Offset: ', self.offsetBox)
        self.settingsLayout.addRow('Frequency: ', self.frequencyBox)
        self.settingsLayout.addRow('Invert: ', self.invertBox)
        self.settingsBox.setLayout(self.settingsLayout)
        self.settingsBox.setFixedWidth(200)
        self.settingsBox.setDisabled(1)

        #Creating data table
        self.dataTable = QTableWidget()
        self.dataTable.setRowCount(10)
        self.dataTable.setColumnCount(2)
        self.dataTable.setHorizontalHeaderLabels(('Time', 'Voltage'))
        self.dataTable.resizeColumnsToContents()
        self.dataTable.setFixedWidth(200)
        self.dataTable.setDisabled(1)

        #Creating waveform preview screen
        self.screen = waveformPreview()
        self.screen.waveformPlot()
        self.screen.setFixedWidth(400)
        self.screen.setFixedHeight(400)
        self.screenLayout = QVBoxLayout()
        self.screenLayout.addWidget(self.screen.canvas)
        self.screen.setLayout(self.screenLayout)

        #Formatting data row
        self.dataLayout = QHBoxLayout()
        self.dataLayout.addWidget(self.settingsBox)
        self.dataLayout.addWidget(self.dataTable)
        self.dataLayout.addWidget(self.screen)
        self.dataLayout.addStretch(1)

        #--Bottom Row: Sending Data---#
        #Generating widgets

        #Button for saving AFG configuration
        self.saveConfigButton = QPushButton('Save AFG Configuration')
        self.saveConfigButton.setDisabled(1)
        self.saveConfigButton.clicked.connect(self.save_config)
        self.saveConfigButton.setFixedHeight(50)
        self.saveConfigButton.setFixedWidth(135)

        #Button for loading AFG configuration
        self.loadConfigButton = QPushButton('Load AFG Configuration')
        self.loadConfigButton.setDisabled(1)
        self.loadConfigButton.clicked.connect(self.load_config)
        self.loadConfigButton.setFixedHeight(50)
        self.loadConfigButton.setFixedWidth(135)
        

        #Button for sending waveform to function generator.
        self.sendButton = QPushButton('Send Waveform to Function Generator')
        self.sendButton.setDisabled(1)
        self.sendButton.clicked.connect(self.send_wave)
        self.sendButton.setFixedHeight(50)
        self.sendButton.setFixedWidth(200)

        #Formatting widgets
        self.sendLayout = QHBoxLayout()
        self.sendLayout.addWidget(self.saveConfigButton, alignment=Qt.AlignRight)
        self.sendLayout.addWidget(self.loadConfigButton, alignment=Qt.AlignRight)
        self.sendLayout.addWidget(self.sendButton, alignment=Qt.AlignRight)
        self.sendLayout.setAlignment(Qt.AlignRight)

        #---Formatting Widget---#
        self.Layout = QVBoxLayout()
        self.Layout.addLayout(self.chooseLayout)
        self.Layout.addLayout(self.dataLayout)
        self.Layout.addLayout(self.sendLayout)
        self.setLayout(self.Layout)
        self.setFixedWidth(825)
        self.setFixedHeight(520)
        

    #Functions
    @Slot()
    def save_config(self):

        '''
        This function saves the function generator configuration to a .txt file.
        '''

        chooseSaveData = QFileDialog()
        chooseSaveData.setAcceptMode(QFileDialog.AcceptSave)
        chooseSaveData.setDefaultSuffix(unicode('txt'))
        if chooseSaveData.exec_():
            saveName = chooseSaveData.selectedFiles()
            saveName = saveName[0]
            saveName.encode('ascii', 'ignore')
            try:
                export_settings(saveName, self.parent.AFG)
                return
            except:
                self.nameerror.emit()
                return

    @Slot()
    def load_config(self):

        '''
        This function loads a previous configuration to the function generator from a .txt file.
        '''

        chooseFile = QFileDialog()
        chooseFile.setNameFilter('Text Files (*.txt)')
        if chooseFile.exec_():
            fileName = chooseFile.selectedFiles()
            fileName = fileName[0]
            fileName.encode('ascii', 'ignore')
            try:
                import_settings(fileName, self.parent.AFG)
                return
            except:
                self.nameerror.emit()
                return
            
    @Slot()
    def send_wave(self):

        '''
        The action performed by sendButton. send_wave checks to make sure that the user input is
        formatted correctly, and then calls the loadwaveform function to send the user input to the
        function generator.

        Arguments: None
        '''

        if self.chooseWaveBox.currentIndex() == -1:
            self.parent.afg_waveformerror()
            return
        
        if self.swedenBox.currentIndex() == -1:
            self.parent.afg_swedenerror()
            return
            
        if self.timeBox.currentIndex() == -1:
            self.parent.afg_tunitserror()
            return
            
        if self.voltBox.currentIndex() == -1:
            self.parent.afg_vunitserror()
            return

        wavetype = self.chooseWaveDict[self.chooseWaveBox.currentText().encode('ascii', 'ignore')]
        tunits = self.timeDict[self.timeBox.currentText().encode('ascii', 'ignore')]
        vunits = self.voltDict[self.voltBox.currentText().encode('ascii', 'ignore')]
        source_num = int(self.channelBox.currentIndex()) + 1

        #If the wavetype is arbitrary...
        if wavetype == 'ememory':

            if not self.loadedWave:
                self.parent.afg_nofileerror()
                return

            #Checking to make sure that the resolution is a number.
            try:
                resolution = int(self.resolutionBox.text())
            except:
                self.parent.afg_resolutionerror()
                return
                
            #Checking to make sure that the resolution is within the appropriate bounds.
            if resolution <=2 or resolution >= 131072:
                self.parent.afg_resolutionerror()
                return

            if self.swedenBox.currentText().encode('ascii', 'ignore') == 'Yes':
                sweden = 'no'
            else:
                sweden = 'yes'                
            
            loadwaveform(self.parent.AFG, wavetype, imported=self.waveformData,
                         source=source_num, resolution=resolution, tunits=tunits,
                         vunits=vunits, sweden=sweden, invert=self.invertBox.isChecked())
            
        #If the wavetype is not arbitrary...
        else:

            #Checking to make sure that the frequency is a number.
            try:
                frequency = float(self.frequencyBox.text())
            except:
                self.parent.afg_frequencyerror()
                return

            #Checking to make sure that the frequency is positive.
            if frequency <= 0.0:
                self.parent.afg_frequencyerror()
                return

            #Checking to make sure that the offset is a number.
            try:
                offset = float(self.offsetBox.text())
            except:
                self.parent.afg_offseterror()
                return

            #Checking to make sure that the amplitude is a number.
            try:
                amplitude = float(self.amplitudeBox.text())
            except:
                self.parent.afg_amplitudeerror()
                return

            #Checking to make sure that the amplitude is positive.
            if amplitude <= 0.0:
                self.parent.afg_amplitudeerror()
                return

            if self.swedenBox.currentText().encode('ascii', 'ignore') == 'Yes':
                sweden = 'no'
            else:
                sweden = 'yes'

            loadwaveform(self.parent.AFG, wavetype, freq=frequency,
                         offset=offset, vpp=amplitude, source=source_num, tunits=tunits,
                         vunits=vunits, sweden=sweden, invert=self.invertBox.isChecked())

    @Slot()
    def choose_waveform(self):

        '''
        The action performed by chooseWaveBox. Enables the relevant user options based on
        the waveform type that has been selected. Also displays data using the set_table_screen
        function.

        Arguments: None
        '''
        
        if self.chooseWaveBox.currentText().encode('ascii', 'ignore') == 'Arb':
            self.getWaveform.setEnabled(1)
            self.resolutionBox.setEnabled(1)
            self.frequencyBox.setDisabled(1)
            self.amplitudeBox.setDisabled(1)
            self.offsetBox.setDisabled(1)
            

            #If a wave has already been loaded in the past, the waveform is retrieved and displayed
            #in the preview screen/table.
            if self.loadedWave:
                self.set_table_screen()
                self.amplitudeBox.setText(str(self.waveformData['rangeref']))
                self.frequencyBox.setText(str(self.waveformData['frequency']))
                self.offsetBox.setText(str(self.waveformData['midref']))
        else:
            self.getWaveform.setDisabled(1)
            self.resolutionBox.setDisabled(1)
            self.frequencyBox.setEnabled(1)
            self.amplitudeBox.setEnabled(1)
            self.offsetBox.setEnabled(1)
            self.set_table_screen()

        self.settingsBox.setEnabled(1)
    
    @Slot()
    def load_wave(self):

        '''
        The action performed by the getWaveform button. Opens up a file dialog that asks the user
        to load a text file formatted for the importdata function. If the file that is selected is
        not formatted correctly, the user is presented an error message. If file is loaded successfully,
        data from the file is stored, and the application is set to display relevant data from the file.

        Arguments: None
        '''
        
        chooseLoadWave = QFileDialog()
        chooseLoadWave.setNameFilter('Text Files (*.txt)')
        if chooseLoadWave.exec_():
            loadWaveName = chooseLoadWave.selectedFiles()
            loadWaveName = loadWaveName[0]
            loadWaveName.encode('ascii', 'ignore')

            try:
                self.waveformData = importdata(loadWaveName)
            except:
                self.nameerror.emit()
                return

            self.loadedWave = True
            self.amplitudeBox.setText(str(self.waveformData['rangeref']))
            self.frequencyBox.setText(str(self.waveformData['frequency']))
            self.offsetBox.setText(str(self.waveformData['midref']))                                       
            self.set_table_screen()

    @Slot()
    def set_table_screen(self):

        '''
        Function that sets the graph/chart according to the waveform type that has been selected.
        For arbs, it loads the table and plots an arbitrary waveform. For non-arbs, it clears the
        table and displays on of the premade waves in waveformPreview.

        Arguments:

            wavetype: The waveform type that has been selected.
        '''
        wavetype = self.chooseWaveBox.currentText().encode('ascii', 'ignore')
        invert = self.invertBox.isChecked()
        
        self.dataTable.clearContents()
        if wavetype == 'Arb':
            self.dataTable.setEnabled(1)
            self.dataTable.setRowCount(len(self.waveformData['refform']))
            for i in range(len(self.waveformData['refform'])):
                tableItem = QTableWidgetItem(str(self.waveformData['timevals'][i]))
                tableItem.setFlags(tableItem.flags() != Qt.ItemIsEditable)
                self.dataTable.setItem(i, 0, tableItem)
                tableItem = QTableWidgetItem(str(self.waveformData['refform'][i]))
                tableItem.setFlags(tableItem.flags() != Qt.ItemIsEditable)
                self.dataTable.setItem(i, 1, tableItem)
            self.screen.waveformPlot(wavetype, self.waveformData['timevals'], self.waveformData['refform'], invert=invert)

            self.set_units()
            self.dataTable.resizeColumnsToContents()
        else:

            self.dataTable.setDisabled(1)
            amplitude = eval('self.screen.' + wavetype)
            if invert: amplitude = amplitude * -1
            
            if self.amplitudeBox.text() != '':
                try: vpp = abs(float(self.amplitudeBox.text()))
                except: vpp = 1
            else: vpp = 1

            if self.offsetBox.text() != '':
                try: offset = abs(float(self.offsetBox.text()))
                except: offset = 0.0
            else: offset = 0.0

            if self.frequencyBox.text() != '':
                try: period = 1/float(self.frequencyBox.text())
                except: period = 1
            else: period = 1

            time = eval('self.screen.' + wavetype + 'time * period')
            amplitude = (amplitude * vpp) + offset
            self.screen.waveformPlot('Nonarb', time, amplitude, invert=invert)
            self.set_units()

    @Slot()
    def set_units(self):

            '''
            This function changes the units displayed on the waveform preview.
            '''
        
            volt = self.voltBox.currentText().encode('ascii', 'ignore').strip()
            try: vunits = volt[volt.index('('):len(volt)]
            except: vunits = ''
            
            time = self.timeBox.currentText().encode('ascii', 'ignore').strip()
            try: tunits = time[time.index('('):len(time)]
            except: tunits = ''
                    
            self.dataTable.setHorizontalHeaderLabels(('Time %s' %tunits, 'Voltage %s' %vunits))
            self.screen.axes.set_xlabel('Time %s' %tunits)
            self.screen.axes.set_ylabel('Amplitude %s' %vunits)
            self.screen.canvas.draw()
            
#For previewing the tab outside of the main application.
if __name__ == '__main__':
    qtApp = QApplication(sys.argv)
    waveformWindow = waveformTab()
    waveformWindow.show()
    qtApp.exec_()
